package oracle

import (
	"math"

	"github.com/jinzhu/copier"

	"gitlab.com/psns/sie/app/physx"
)

// Frame define ..
type Frame struct {
	Particles []*physx.Particle `json:"particles"`
}

// NewFrame create and return a new frame.
func NewFrame() Frame {
	return Frame{
		Particles: []*physx.Particle{},
	}
}

// GenerateNextFrame generate a next frame.
func (f *Frame) GenerateNextFrame(shapes []physx.Shape, timeScale float64) *Frame {
	// Create deep copy of initial frame array
	particles := make([]*physx.Particle, 0)
	for _, particle := range f.Particles {
		var p physx.Particle
		_ = copier.Copy(&p, particle)
		particles = append(particles, &p)
	}

	// Reset Acc of each particle of copied array and compute next their pos
	f.resetParticlesAcc(particles)
	f.preCalculate(particles, shapes, timeScale)

	return &Frame{Particles: particles}
}

// resetParticlesAcc set to 0 acceleration of all particles.
func (f *Frame) resetParticlesAcc(particles []*physx.Particle) {
	for _, particle := range particles {
		particle.A.Vx, particle.A.Vy = 0, 0
	}
}

// preCalculate speed and acceleration of all particles for the next frame.
// Does not apply position.
func (f *Frame) preCalculate(particles []*physx.Particle, shapes []physx.Shape, timeScale float64) {
	var particleB *physx.Particle
	var tempAcc physx.Vector

	preCalculated := make([]*physx.Particle, 0)

	for index, particleA := range particles {
		for i := index + 1; i < len(particles); i++ {
			particleB = particles[i]

			// Calc coulomb force
			tempAcc = physx.CoulombForce(particleA, particleB)

			// Add acc to particle A
			particleA.A.Vx += tempAcc.Vx
			particleA.A.Vy += tempAcc.Vy
			// Pre add acc to particle B
			particleB.A.Vx -= tempAcc.Vx
			particleB.A.Vy -= tempAcc.Vy
		}
		// Apply second law of newton
		particleA.A.Vx /= particleA.M
		particleA.A.Vy /= particleA.M
		// Calc speed at next frame
		particleA.VNext.Vx = particleA.V.Vx + particleA.A.Vx*timeScale
		particleA.VNext.Vy = particleA.V.Vy + particleA.A.Vy*timeScale
		preCalculated = append(preCalculated, particleA)
	}
	f.applyPosition(preCalculated, shapes, timeScale)
}

// applyPosition of all particles.
func (f *Frame) applyPosition(particles []*physx.Particle, shapes []physx.Shape, timeScale float64) {
	var particle *physx.Particle
	var xMov float64
	var yMov float64
	var finalPoint physx.Point
	var initialIn bool

	for i := 0; i < len(particles); i++ {
		if particles[i].Locked {
			continue
		}
		particle = particles[i]

		xMov = particle.V.Vx*timeScale + (particle.A.Vx/2)*math.Pow(timeScale, 2)
		yMov = particle.V.Vy*timeScale + (particle.A.Vy/2)*math.Pow(timeScale, 2)

		finalPoint = physx.NewPoint(particle.Pos.X+xMov, particle.Pos.Y+yMov)

		for _, shape := range shapes {
			initialIn = shape.IsInside(particle.Pos)

			if initialIn != shape.IsInside(finalPoint) {
				finalPoint = shape.ManageCollision(particle.Pos, finalPoint, initialIn)
			}
		}

		particle.Pos = finalPoint
		particle.V = particle.VNext
	}
}
