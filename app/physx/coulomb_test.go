package physx

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCoulombLaw(t *testing.T) {
	c1 := NewParticle(0, 0, QELECTRON, MELECTRON)
	c2 := NewParticle(1e-7, 0, QPROTON, MPROTON)

	assert.InDelta(t, -2.30e-8, CoulombLaw(c1, c2), 1e-8)
}

func TestCoulombForce(t *testing.T) {
	c1 := NewParticle(0, 0, QELECTRON, MELECTRON)
	c2 := NewParticle(1, 1, QPROTON, MELECTRON)

	f := CoulombForce(c1, c2)
	assert.InDelta(t, 8e-23, f.Vx, 1e-23)
	assert.InDelta(t, 8e-23, f.Vy, 1e-23)
}
