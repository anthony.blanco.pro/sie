package physx

const (
	// QPROTON is the value of Proton Charge.
	QPROTON = 1.6e-16

	// QELECTRON is the value of Electron Charge.
	QELECTRON = -1.6e-16

	// MPROTON mass of a proton.
	MPROTON = 1.6726e-27

	// MELECTRON mass of an electron.
	MELECTRON = 9.1094e-31
)

// Particle define a charge with a charge and a mass.
type Particle struct {
	Pos    Point   `json:"pos"`    // Pos position of the particle charge
	Q      float64 `json:"q"`      // Q value of the particle charge
	M      float64 `json:"m"`      // M mass of the particle charge
	A      Vector  `json:"a"`      // A acc of the particle
	V      Vector  `json:"v"`      // V velocity of the particle
	VNext  Vector  `json:"-"`      // Velocity at next frame
	Locked bool    `json:"locked"` // Is particle is lock (can't move)
}

// NewParticle create and return a new instance of Particle.
func NewParticle(x, y, q, m float64) *Particle {
	return &Particle{
		Pos:    NewPoint(x, y),
		Q:      q,
		M:      m,
		A:      NewVectorByCoordinate(0, 0),
		V:      NewVectorByCoordinate(0, 0),
		VNext:  NewVectorByCoordinate(0, 0),
		Locked: false,
	}
}
