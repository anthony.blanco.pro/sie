package physx

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewParticle(t *testing.T) {
	cp := NewParticle(1, 0, QPROTON, MPROTON)
	ce := NewParticle(-1, 5, QELECTRON, MELECTRON)

	assert.Equal(t, 1.6e-16, cp.Q)
	assert.Equal(t, float64(1), cp.Pos.X)
	assert.Equal(t, float64(0), cp.Pos.Y)

	assert.Equal(t, -1.6e-16, ce.Q)
	assert.Equal(t, float64(-1), ce.Pos.X)
	assert.Equal(t, float64(5), ce.Pos.Y)
}
