package physx

import "math"

// Point define a 2D point of space.
type Point struct {
	X float64 `json:"x"` // X coordinate in meters
	Y float64 `json:"y"` // Y coordinate in meters
}

// NewPoint create and return a new instance of Point.
func NewPoint(x, y float64) Point {
	return Point{X: x, Y: y}
}

// GetDistance return the distance between two points.
func GetDistance(p1, p2 Point) float64 {
	return math.Sqrt(math.Pow(p1.X-p2.X, 2) + math.Pow(p1.Y-p2.Y, 2))
}
