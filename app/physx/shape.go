package physx

import (
	"math"
)

// Pi2 define math.Pi/2 const.
const Pi2 = math.Pi / 2

// epsilon is a value to add to a particle pos to avoid setting it on a shape surface.
const epsilon = 1e-10

// JSONShape struct of the JSON shape.
type JSONShape struct {
	Type string             `json:"type"`
	Info map[string]float64 `json:"info"`
}

// Shape define generic shape functions.
type Shape interface {
	shape()
	IsInside(p Point) bool
	ManageCollision(initial, final Point, initialIn bool) Point
}

// Convert json byte to struct.
func Convert(jShape JSONShape) Shape {
	switch jShape.Type {
	case "CIRCLE":
		center := NewPoint(jShape.Info["center_x"], jShape.Info["center_y"])
		return NewCircle(center, jShape.Info["radius"])
	case "RECTANGLE":
		p1 := NewPoint(jShape.Info["p1_x"], jShape.Info["p1_y"])
		p2 := NewPoint(jShape.Info["p2_x"], jShape.Info["p2_y"])
		return NewRectangle(p1, p2)
	}
	return nil
}

// ############## Circle ###############

// Circle define a circle with a center and a radius.
type Circle struct {
	C *Point  `json:"center"`
	R float64 `json:"radius"`
}

// NewCircle create a new circle with a center and a radius.
func NewCircle(center Point, radius float64) Circle {
	return Circle{C: &center, R: radius}
}

// shape define the function of interface.
func (c Circle) shape() {}

// IsInside check is a point p is inside a circle defined with 2 points.
func (c Circle) IsInside(p Point) bool {
	return GetDistance(*c.C, p) <= c.R
}

// ManageCollision with a given initial and final Point and a Circle.
func (c Circle) ManageCollision(_, final Point, initialIn bool) Point {
	var epsiVal = epsilon
	var newFinal Point

	arc := NewVectorByPoint(*c.C, final).DirectionAngle()

	if initialIn {
		epsiVal *= -1
	}

	newFinal.X, newFinal.Y = c.C.X+math.Cos(arc)*(c.R+epsiVal), c.C.Y+math.Sin(arc)*(c.R+epsiVal)

	return newFinal
}

// ############## Rectangle ###############

// Rectangle define a quadruped with 2 points.
type Rectangle struct {
	P1, P2 *Point
}

// NewRectangle create a new rectangle with 2 points.
func NewRectangle(point1, point2 Point) Rectangle {
	return Rectangle{P1: &point1, P2: &point2}
}

// shape define the function of interface.
func (r Rectangle) shape() {}

// IsInside check is a point p is inside a rectangle defined with 2 points.
func (r Rectangle) IsInside(p Point) bool {
	return p.X <= r.P1.X && p.X >= r.P2.X && p.Y >= r.P1.Y && p.Y <= r.P2.Y
}

// findFace return a vector corresponding to the face hit by a particle.
// nolint: unused
func (r Rectangle) findFace(initial, final Point, initialIn bool) Vector {
	var face Vector

	insidePoint := final
	if !initialIn {
		insidePoint = initial
	}

	if insidePoint.Y > r.P2.Y {
		face = NewVectorByPoint(NewPoint(r.P1.X, r.P2.Y), *r.P2) // Top face
	} else if insidePoint.Y < r.P1.Y {
		face = NewVectorByPoint(*r.P1, NewPoint(r.P2.X, r.P1.Y)) // Bottom face
	} else if insidePoint.X > r.P2.X {
		face = NewVectorByPoint(NewPoint(r.P2.X, r.P1.Y), *r.P2) // Right face
	} else {
		face = NewVectorByPoint(*r.P1, NewPoint(r.P1.X, r.P2.Y)) // Left face
	}

	return face
}

// ManageCollision with a given initial and final Point and a Rectangle.
func (r Rectangle) ManageCollision(initial, final Point, initialIn bool) Point {
	return final
}
