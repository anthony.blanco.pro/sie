package physx

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewCircle(t *testing.T) {
	p := NewPoint(-5, 2)
	cir := NewCircle(p, 5)

	assert.Equal(t, float64(-5), cir.C.X)
	assert.Equal(t, float64(2), cir.C.Y)
	assert.Equal(t, float64(5), cir.R)
}

func TestCircle_IsInside(t *testing.T) {
	p := NewPoint(0, 0)
	cir := NewCircle(p, 5)

	assert.Equal(t, true, cir.IsInside(NewPoint(0, 0)))
	assert.Equal(t, true, cir.IsInside(NewPoint(0, 5)))
	assert.Equal(t, true, cir.IsInside(NewPoint(5, 0)))
	assert.Equal(t, true, cir.IsInside(NewPoint(3, 2)))
	assert.Equal(t, true, cir.IsInside(NewPoint(0, -5)))
	assert.Equal(t, true, cir.IsInside(NewPoint(-5, 0)))
	assert.Equal(t, true, cir.IsInside(NewPoint(-1, 2)))
	assert.Equal(t, false, cir.IsInside(NewPoint(5, 5)))
	assert.Equal(t, false, cir.IsInside(NewPoint(-5, -5)))
	assert.Equal(t, false, cir.IsInside(NewPoint(6, 0)))
	assert.Equal(t, false, cir.IsInside(NewPoint(0, 6)))
	assert.Equal(t, false, cir.IsInside(NewPoint(-6, 6)))
	assert.Equal(t, false, cir.IsInside(NewPoint(-6, -7)))
}

func TestCircle_ManageCollision(t *testing.T) {
	cir := NewCircle(NewPoint(0, 0), 10)
	initial := NewPoint(0, 0) // Initial point doesn't matter for Circle collision

	newFinal := cir.ManageCollision(initial, NewPoint(0, 5), false)
	assert.InDelta(t, float64(0), newFinal.X, 1e-15)
	assert.Equal(t, 10+epsilon, newFinal.Y)

	newFinal = cir.ManageCollision(initial, NewPoint(0, 5), true)
	assert.InDelta(t, float64(0), newFinal.X, 1e-15)
	assert.Equal(t, 10-epsilon, newFinal.Y)

	newFinal = cir.ManageCollision(initial, NewPoint(5, 0), true)
	assert.InDelta(t, float64(0), newFinal.Y, 1e-15)
	assert.Equal(t, 10-epsilon, newFinal.X)

	newFinal = cir.ManageCollision(initial, NewPoint(5, 0), true)
	assert.InDelta(t, float64(0), newFinal.Y, 1e-15)
	assert.Equal(t, 10-epsilon, newFinal.X)
}

func TestNewRectangle(t *testing.T) {
	p1 := NewPoint(0, 1)
	p2 := NewPoint(6, 5)
	rec := NewRectangle(p1, p2)

	assert.Equal(t, float64(0), rec.P1.X)
	assert.Equal(t, float64(1), rec.P1.Y)
	assert.Equal(t, float64(6), rec.P2.X)
	assert.Equal(t, float64(5), rec.P2.Y)
}

func TestRectangle_IsInside(t *testing.T) {
	p1 := NewPoint(5, 0)
	p2 := NewPoint(0, 5)
	rec := NewRectangle(p1, p2)

	assert.Equal(t, true, rec.IsInside(NewPoint(0, 0)))
	assert.Equal(t, true, rec.IsInside(NewPoint(0, 5)))
	assert.Equal(t, true, rec.IsInside(NewPoint(1, 4)))
	assert.Equal(t, false, rec.IsInside(NewPoint(-1, 0)))
	assert.Equal(t, false, rec.IsInside(NewPoint(5, 6)))
	assert.Equal(t, false, rec.IsInside(NewPoint(3, 6)))
}

func TestJSONShape_Convert(t *testing.T) {
	jsonInputCircle := []byte(`{
		"type": "CIRCLE",
		"info": {
			"center_x": 0,
			"center_y": 0,
			"radius": 5
		}
	}`)

	var sCir JSONShape
	err := json.Unmarshal(jsonInputCircle, &sCir)
	assert.Nil(t, err)

	cir := Convert(sCir)
	assert.Equal(t, true, cir.IsInside(NewPoint(0, 0)))
	assert.Equal(t, true, cir.IsInside(NewPoint(5, 0)))
	assert.Equal(t, false, cir.IsInside(NewPoint(5, 5)))

	jsonInputRectangle := []byte(`{
		"type": "RECTANGLE",
		"info": {
			"p1_x": 2,
			"p1_y": 0,
			"p2_x": 0,
			"p2_y": 5
		}
	}`)

	var sRec JSONShape
	err = json.Unmarshal(jsonInputRectangle, &sRec)
	assert.Nil(t, err)

	rec := Convert(sRec)
	assert.Equal(t, true, rec.IsInside(NewPoint(0, 0)))
	assert.Equal(t, true, rec.IsInside(NewPoint(1, 1)))
	assert.Equal(t, false, rec.IsInside(NewPoint(-2, 0)))
}
