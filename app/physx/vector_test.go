package physx

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewVectorByPoint(t *testing.T) {
	a := NewPoint(1, 0)
	b := NewPoint(0, 1)

	v := NewVectorByPoint(a, b)

	assert.Equal(t, float64(-1), v.Vx)
	assert.Equal(t, float64(1), v.Vy)
}

func TestNewVectorByCoordinate(t *testing.T) {
	v := NewVectorByCoordinate(0, 1)

	assert.Equal(t, float64(0), v.Vx)
	assert.Equal(t, float64(1), v.Vy)
}

func TestScalarProduct(t *testing.T) {
	vec1 := NewVectorByCoordinate(1, 1)
	vec2 := NewVectorByCoordinate(2, 3)
	scal := ScalarProduct(&vec1, &vec2)

	assert.Equal(t, float64(3), scal)
}

func TestMultiplyVector(t *testing.T) {
	vec1 := NewVectorByCoordinate(1, 2)
	mult := MultiplyVector(&vec1, float64(3))

	assert.Equal(t, float64(3), mult.Vx)
	assert.Equal(t, float64(6), mult.Vy)
}

func TestVectorGetDirectionAngle(t *testing.T) {
	vec1 := NewVectorByCoordinate(3, 3)
	vec2 := NewVectorByCoordinate(3, -3)
	vec3 := NewVectorByCoordinate(-3, 3)
	vec4 := NewVectorByCoordinate(-3, -3)

	assert.Equal(t, math.Pi/4, vec1.DirectionAngle())
	assert.Equal(t, -1*math.Pi/4, vec2.DirectionAngle())
	assert.Equal(t, 3*math.Pi/4, vec3.DirectionAngle())
	assert.Equal(t, -3*math.Pi/4, vec4.DirectionAngle())
}
