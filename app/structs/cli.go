package structs

import "github.com/alexflint/go-arg"

// Args define a cli struct.
var Args struct {
	NoServe bool   `arg:"-n, --no-serv" help:"disable serving server ws of vue app"`
	NoCors  bool   `arg:"-c, --no-cors" help:"disable CORS policy"`
	Port    string `arg:"-p, --port" help:"change the default port of the ws"`
	Addr    string `arg:"-a, --addr" help:"set the ip address of the server"`
}

// ParseArgs of the cli.
func ParseArgs() {
	Args.Port = "8080"
	Args.Addr = "127.0.0.1"
	Args.NoCors = false
	arg.MustParse(&Args)
}
