package structs

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"

	"gitlab.com/psns/sie/app/utils"

	"github.com/gorilla/websocket"
)

// Client create a new basic client.
type Client struct {
	wsCon *websocket.Conn
}

// NewClient create a new client.
func NewClient(conn *websocket.Conn) *Client {
	return &Client{
		wsCon: conn,
	}
}

// Listen to sent messages.
func (c *Client) Listen(p *Pool) {
	defer func() {
		p.RemoveClient(c)
		c.wsCon.Close()
	}()

	// Handle client connection, read and treat sent messages.
	for {
		_, data, errRead := c.wsCon.ReadMessage()
		if errRead != nil {
			return
		}

		c.onMessageReceived(data)
	}
}

// SendData send data to the client.
func (c *Client) SendData(messageType StatusType, route string, data interface{}) {
	msg, _ := json.Marshal(NewMessage(messageType, utils.NoErr, route, data))

	if err := c.wsCon.WriteMessage(websocket.TextMessage, msg); err != nil {
		log.Printf("Unable to send message: %s", err.Error())
	}
}

// SendError to the client.
func (c *Client) SendError(route string, sError *utils.SError) {
	msg, _ := json.Marshal(NewMessage(MessageClose, sError.Code, route, sError.Error()))
	if err := c.wsCon.WriteMessage(websocket.TextMessage, msg); err != nil {
		log.Printf("Unable to send error: %s", err.Error())
	}
}

// onMessageReceived is called when the client sent a message.
func (c *Client) onMessageReceived(data []byte) {
	var msg Message
	if err := json.Unmarshal(data, &msg); err != nil {
		c.SendError("", utils.NewSError(utils.BadRequestFormat, err))
		return
	}

	if msg.Status == MessageRequest {
		switch msg.Route {
		case "list":
			c.listAllProject(&msg)
		case "load":
			c.loadProject(&msg)
		case "save":
			c.saveProject(&msg)
		case "edit":
			c.editProject(&msg)
		case "remove":
			c.removeProject(&msg)
		case "render":
			c.renderSimulation(&msg)
		}
	}
}

// listAllProject present in the save dir.
func (c *Client) listAllProject(msg *Message) {
	fileListStr := make([][]string, 0)
	fileList, err := utils.ListProjects()

	if err != nil {
		c.SendError(msg.Route, err)
	} else {
		for fileName, fileInfo := range fileList {
			fileListStr = append(fileListStr, []string{fileName, fileInfo.ModTime().String(), fmt.Sprintf("%d", fileInfo.Size())})
		}
		c.SendData(MessageClose, msg.Route, fileListStr)
	}
}

// loadProject and return details to the client.
func (c *Client) loadProject(msg *Message) {
	var prj Project
	if filePath, ok := msg.Data.(string); ok {
		if err := prj.Load(filePath); err != nil {
			c.SendError(msg.Route, err)
		} else {
			c.SendData(MessageClose, msg.Route, prj)
		}
	} else {
		c.SendError(msg.Route, utils.NewSError(utils.MessageCodeInvalidData, nil))
	}
}

// saveProject in the save dir.
func (c *Client) saveProject(msg *Message) {
	var prj Project

	if stringifyProject, ok := msg.Data.(string); ok {
		if err := json.Unmarshal([]byte(stringifyProject), &prj); err != nil {
			c.SendError(msg.Route, utils.NewSError(utils.MessageCodeInvalidData, err))
		} else if saveErr := prj.Save(); saveErr != nil {
			c.SendError(msg.Route, saveErr)
		} else {
			c.SendData(MessageClose, msg.Route, "")
		}
	} else {
		c.SendError(msg.Route, utils.NewSError(utils.MessageCodeInvalidData, nil))
	}
}

// renderSimulation compute simulation frames.
func (c *Client) renderSimulation(msg *Message) {
	var prj Project

	if stringifyProject, ok := msg.Data.(string); ok {
		if err := json.Unmarshal([]byte(stringifyProject), &prj); err != nil {
			c.SendError(msg.Route, utils.NewSError(utils.MessageCodeInvalidData, err))
		} else {
			prj.Render()
			c.SendData(MessageClose, msg.Route, prj)
		}
	} else {
		c.SendError(msg.Route, utils.NewSError(utils.MessageCodeInvalidData, nil))
	}
}

// editProject in the save name of an project.
func (c *Client) editProject(msg *Message) {
	var prj Project

	if namesArray, ok := msg.Data.([]interface{}); ok {
		if len(namesArray) != 2 {
			c.SendError(msg.Route, utils.NewSError(utils.EditProjectKo, errors.New("invalid data number. Only old[0] and new[1] name should be given in array")))
		} else if errLoad := prj.Load(namesArray[0].(string)); errLoad != nil {
			c.SendError(msg.Route, utils.NewSError(utils.LoadFileKo, errLoad))
		} else if errEdit := prj.Edit(namesArray[1].(string)); errEdit != nil {
			c.SendError(msg.Route, utils.NewSError(utils.EditProjectKo, errEdit))
		} else {
			c.SendData(MessageClose, msg.Route, "")
		}
	} else {
		c.SendError(msg.Route, utils.NewSError(utils.MessageCodeInvalidData, nil))
	}
}

// removeProject from save dir.
func (c *Client) removeProject(msg *Message) {
	var prj Project

	if filename, ok := msg.Data.(string); ok {
		if loadErr := prj.Load(filename); loadErr != nil {
			c.SendError(msg.Route, loadErr)
		} else if rmErr := prj.Remove(); rmErr != nil {
			c.SendError(msg.Route, rmErr)
		} else {
			c.SendData(MessageClose, msg.Route, "")
		}
	} else {
		c.SendError(msg.Route, utils.NewSError(utils.MessageCodeInvalidData, nil))
	}
}
