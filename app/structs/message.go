package structs

import (
	"gitlab.com/psns/sie/app/utils"
)

// StatusType define a message status.
type StatusType string

// Message status.
const (
	MessageRequest  StatusType = "REQUEST"
	MessageResponse StatusType = "RESPONSE"
	MessageClose    StatusType = "CLOSE"
)

// Message is response json format.
type Message struct {
	Status StatusType    `json:"status"`
	Code   utils.ErrCode `json:"code"`
	Route  string        `json:"route"`
	Data   interface{}   `json:"data"`
}

// NewMessage create and return a new Message.
func NewMessage(status StatusType, code utils.ErrCode, route string, data interface{}) *Message {
	return &Message{
		Status: status,
		Code:   code,
		Route:  route,
		Data:   data,
	}
}
