package structs

import (
	"fmt"
	"os"
)

// Pool define a pool.
type Pool struct {
	ID      int       // Id of the pool
	Name    string    // Name of the pool
	clients []*Client // Client list connected to the pool
}

// NewPool Create a new pool and init channels.
func NewPool(poolID int, poolName string) *Pool {
	return &Pool{
		ID:      poolID,
		Name:    poolName,
		clients: make([]*Client, 0),
	}
}

// AddClient add a client to the pool.
func (p *Pool) AddClient(client *Client) {
	p.clients = append(p.clients, client)
	fmt.Printf("A new client [%d] as join the pool\n", len(p.clients))
}

// RemoveClient remove the given client to the pool.
func (p *Pool) RemoveClient(client *Client) {
	for idx, cl := range p.clients {
		if cl == client {
			p.clients = append(p.clients[:idx], p.clients[idx+1:]...)
			fmt.Printf("A new client idx:%d as leave the pool\n", idx)
			os.Exit(0)
		}
	}
}
