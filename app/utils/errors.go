package utils

import "fmt"

// ErrCode define a SIE error code.
type ErrCode int

// App error codes.
const (
	NoErr ErrCode = iota + 100
	BadRequestFormat
	MessageCodeKo                 // MessageCodeKo : Unknown error
	MessageCodeInvalidData        // MessageCodeInvalidData : Invalid data sent in request
	MessageCodeToMuchClientInPool // MessageCodeToMuchClientInPool : To much client in the pool
	SaveFileKo                    // SaveFileKo : Error while saving file
	SaveFileInvalidData           // SaveFileInvalidData : Data to save is invalid
	SaveFileCreateDirKo           // SaveFileCreateDirKo : Save dir not found
	LoadFileKo                    // LoadFileKo : Error while loading file
	LoadFileNotFound              // LoadFileNotFound : Requested save file not found
	LoadFileReadError             // LoadFileReadError : Unable to read requested save file
	LoadFileDecodeError           // LoadFileDecodeError : Unable to decode requested save file
	LoadFileCannotLoadDir         // LoadFileCannotLoadDir : Unable to load the directory
	ListFileKo                    // ListFileKo : Error while listing files
	EditProjectKo                 // EditProjectKo : Error on project edition computing
	RemoveProjectKo               // RemoveProjectKo : Error while removing project.
	ExportFileOk                  // ExportFileOk : Error while exporting file.
)

var errMap = map[ErrCode]string{
	BadRequestFormat:              "bad request format",
	MessageCodeKo:                 "unknown error",
	MessageCodeInvalidData:        "the given data is incorrect or invalid",
	MessageCodeToMuchClientInPool: "too much client in the pool",
	SaveFileKo:                    "error while saving file",
	SaveFileInvalidData:           "data to save invalid",
	SaveFileCreateDirKo:           "unable to create default save dir",
	LoadFileKo:                    "error while loading file",
	LoadFileNotFound:              "requested save file not found",
	LoadFileReadError:             "unable to read requested save file",
	LoadFileDecodeError:           "unable to decode requested save file",
	LoadFileCannotLoadDir:         "unable to load a directory",
	ListFileKo:                    "error while listing files",
	EditProjectKo:                 "error while editing project",
	RemoveProjectKo:               "error while removing project",
}

// SError is a custom SIE Error.
type SError struct {
	Code   ErrCode
	SysErr error
}

// NewSError create and return a new SError.
func NewSError(code ErrCode, sysErr error) *SError {
	return &SError{
		Code:   code,
		SysErr: sysErr,
	}
}

// Error implement Error func of error interface.
func (s *SError) Error() string {
	return fmt.Errorf(errMap[s.Code]+": %w", s.SysErr).Error()
}
