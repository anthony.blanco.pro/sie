package utils

import (
	"io/ioutil"
	"os"
	"path"
	"regexp"
)

const (
	// DefaultFileDir is the default dir for saved files.
	DefaultFileDir = "saves"

	// DefaultFileExtension is the custom extension of saved files.
	DefaultFileExtension = "siex"
)

// ExportProjectAsCsv export project as CSV.
func ExportProjectAsCsv() *SError {
	return nil
}

// ListProjects list all available project in dir.
func ListProjects() (map[string]os.FileInfo, *SError) {
	sFiles := make(map[string]os.FileInfo)

	workingDir, errGetwd := os.Getwd()
	if errGetwd != nil {
		return nil, NewSError(ListFileKo, errGetwd)
	}

	saveFileDir := path.Join(workingDir, DefaultFileDir)

	fileList, errRead := ioutil.ReadDir(saveFileDir)
	if errRead != nil {
		return nil, NewSError(ListFileKo, errRead)
	}

	for _, sFile := range fileList {
		reg := regexp.MustCompile("." + DefaultFileExtension + "$")
		if reg.Match([]byte(path.Join(sFile.Name()))) {
			sFiles[sFile.Name()] = sFile
		}
	}

	return sFiles, nil
}
