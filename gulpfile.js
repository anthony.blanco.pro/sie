const {task, parallel, series, watch} = require('gulp')
const shell = require('gulp-shell')
const child = require('child_process')

const vueDevServerPort = '8081'
const backendServerPort = '8080'

/**
 * @type {ChildProcessWithoutNullStreams}
 */
let backend = null

task('build:back',  shell.task(['go build -o sie.out']))
task('build:front', shell.task(['vue-cli-service build']))

task('watch:back', () => watch(['app/*.go', 'main.go'], series('build:back', 'spawn')))
task('watch:front', shell.task([`vue-cli-service serve --port ${vueDevServerPort}`]))

task('spawn', done => {
    if (backend !== null) backend.kill()

    backend = child.spawn('./sie.out', ['-p', backendServerPort, '--no-cors', '--no-serv'], {cwd: '.', })
        .on('error', e => process.stderr.write(e.toString()))
        .on('end', () => done())
    backend.stderr.on('data', d => process.stdout.write(d.toString()))
    backend.stdout.on('data', d => process.stdout.write(d.toString()))

    done()
})

/**
 * a l'appel de onSigTerm, envoie d'un SIGINT au serveur biglog
 * et sort du process gulp actuel
 */
function onSigTerm() {
    if (backend !== null) {
        backend.kill();
        backend.on('exit', () => child.spawnSync('kill', ['-s', 'SIGKILL', process.pid]))
    } else {
        child.spawnSync('kill', ['-s', 'SIGKILL', process.pid]);
    }
    backend = null
}

process.on('SIGINT', onSigTerm); // CTRL+C
process.on('SIGTERM', onSigTerm);
process.on('SIGABRT', onSigTerm);

task('build', parallel('build:back', 'build:front'))
task('watch', parallel('watch:back', 'watch:front'))
task('default', series('build:back', 'spawn', 'watch'))
