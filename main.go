package main

import (
	"gitlab.com/psns/sie/app"
	"gitlab.com/psns/sie/app/structs"
)

func main() {
	structs.ParseArgs() // Parse input args
	app.Start()         // Start websocket listener
}
