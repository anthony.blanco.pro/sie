import '@mdi/font/css/materialdesignicons.css'
import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'

Vue.use(Vuetify)

export default new Vuetify({
    theme: {
        dark: true,
        themes: {
            dark: {
                primary: '#222',
                secondary: '#6d5394'
            }
        }
    },
    icons: {
        iconfont: 'mdi',
    },
})
