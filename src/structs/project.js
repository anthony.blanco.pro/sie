import {API, SendRequest} from '@/websocket'
import Particle from '@/utils/elements/particle'
import Circle from '@/utils/elements/shapes/circle'
import Rectangle from '@/utils/elements/shapes/rectangle'

const defaultTime = 2e-13
const defaultTimeScale = 2e-16
const defaultScale = 1e-10

export default class project {
    name = ''
    time = defaultTime
    time_scale = defaultTimeScale
    scale = defaultScale
    particles = []
    shapes = []
    frames = []

    /**
     * Load a project from the given path
     * @param projectName {String}
     * @return {Promise<undefined>}
     * @constructor
     */
    Load(projectName) {
        return new Promise((resolve, reject) => {
            SendRequest(API.loadProject(projectName))
                .then(res => {
                    this.assignProject(res.data)
                    resolve()
                }).catch(e => reject(e))
        })
    }

    /**
     *  Save the project
     */
    Save() {
        return new Promise((resolve, reject) => {
            SendRequest(API.saveProject(JSON.stringify(this)))
                .then(() => resolve())
                .catch(e => reject(e))
        })
    }

    /**
     *  Render the simulation
     */
    Render() {
        return new Promise((resolve) => {
            this.frames = []
            SendRequest(API.renderProject(JSON.stringify(this)))
                .then(res => {
                    this.assignProject(res.data)
                    resolve()
                })
                .catch(e => console.log(e))
        })
    }

    /**
     * Must be called after changing the move scale
     * @constructor
     */
    RefreshElemPos() {
        this.particles.forEach(particle => particle.RefreshScreenPos(this.scale))
    }

    /**
     * Assign a standard project js object to the current Project
     * @param projObject {Object<Project>}
     */
    assignProject(projObject) {
        let tempPart = []
        let tempShape = []
        let tempFrames = []
        Object.assign(this, projObject)

        this.particles.forEach(particle => tempPart.push(new Particle(particle, this.scale)))
        this.particles = tempPart

        tempShape = []
        this.shapes.forEach(shape => {
            switch (shape.type) {
                case 'CIRCLE':
                    tempShape.push(new Circle(shape))
                    break
                case 'RECTANGLE':
                    tempShape.push(new Rectangle(shape))
                    break
            }
        })
        this.shapes = tempShape

        this.frames.forEach(frame => {
            let particles = []
            frame['particles'].forEach(p => particles.push(new Particle(p, this.scale)))
            tempFrames.push({particles})
        })
        this.frames = tempFrames
    }

    resetSim() {
        this.frames = []
    }

    AddParticle(jsObject) {
        this.particles.push(new Particle(jsObject, this.scale))
        this.resetSim()
    }

    DeleteParticle(id) {
        this.particles.splice(id, 1)
        this.resetSim()
    }

    AddCircle(jsObject) {
        this.shapes.push(new Circle(jsObject))
        this.resetSim()
    }

    DeleteShape(id) {
        this.shapes.splice(id, 1)
        this.resetSim()
    }
}