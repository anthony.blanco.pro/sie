import Vue from 'vue'

const appContext = new Vue({
    data: () => ({
        currentProject: null,
        grid: null,
        mode: 0,
        simOn: false,
        simPause: false,
        simFrame: 0,
        errCallback: () => {},
        startSimCallback: () => {}
    }),
    methods: {
        showError(err) {
            if (this.errCallback != null) {
                this.errCallback(err)
            }
        }
    }
})

export default appContext
