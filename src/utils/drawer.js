export default class Drawer {
    ox
    oy
    p

    constructor() {}

    /**
     * Set the p5 drawing instance
     * @param p {p5} p5 instance to draw with
     * @constructor
     */
    SetP(p) {
        this.p = p
    }

    /**
     * Set offset of the drawer
     * @param x {Number}
     * @param y {Number}
     * @constructor
     */
    SetOffset(x, y) {
        this.ox = x
        this.oy = y
    }

    /**
     * Draw a circle
     * Base point is center point
     * @param x {Number} x coordinate of the base point
     * @param y {Number} y coordinate of the base point
     * @param r {Number} r radius of the circle
     */
    Circle(x, y, r) {
        this.p.circle(this.ox + x, this.oy - y, r * 2)
    }

    /**
     * Draw a Rectangle
     * Base point is lower left point
     * @param x1 {Number} x coordinate of the base point
     * @param y1 {Number} y coordinate of the base point
     * @param x2 {Number} x coordinate of the second point
     * @param y2 {Number} y coordinate of the second point
     */
    Rectangle(x1, y1, x2, y2) {
        this.p.rect(this.ox + x1, this.oy - y1, ((this.ox + x2) - (this.ox + x1)), ((this.oy - y2) - (this.oy - y1)))
    }
}
