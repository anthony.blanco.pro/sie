const particleRadius = 13

export default class Particle {
    pos = {x: 0, y: 0}  // define 2D space physical position
    p = {x: 0, y: 0}    // Screen position in pixel
    q = 0               // Charge of position
    m = 0               // Mass of the particle
    a = {vx: 0, vy: 0}  // Acceleration
    v = {vx: 0, vy: 0}  // Velocity
    locked = false      // If particle is locked on screen

    /**
     * @param jsObject
     * @param moveScale
     */
    constructor(jsObject, moveScale) {
        Object.assign(this, jsObject)
        this.RefreshScreenPos(moveScale)
    }

    Draw(drawer) {
        if (this.locked) drawer.p.fill('#9E9E9E')
        else drawer.p.fill(this.q > 0 ? '#c22930' : '#284fbd')
        drawer.p.stroke(0, 0, 0, 0)
        drawer.Circle(this.p.x, this.p.y, particleRadius)
    }

    DrawSelected(drawer) {
        drawer.p.stroke('#bfa211')
        drawer.p.strokeWeight(2)
        drawer.Circle(this.p.x, this.p.y, particleRadius)
    }

    /**
     * Move the particle from screen, adjust physical position
     * @param x {Number} x offset
     * @param y {Number} y offset
     * @param moveScale
     */
    Move(x, y, moveScale) {
        this.p.x += x
        this.p.y += y
        this.pos = {x: this.p.x * moveScale, y: this.p.y * moveScale}
    }

    /**
     * Set the physical position of the particle, adjust screen pos
     * @param x {Number} physical x pos
     * @param y {Number} physical y pos
     * @param moveScale
     */
    SetPhysicalPos(x, y, moveScale) {
        this.pos = {x, y}
        this.RefreshScreenPos(moveScale)
    }

    /**
     * Refresh the position of particles on screen depending on physical position
     */
    RefreshScreenPos(moveScale) {
        this.p = {x: this.pos.x/moveScale, y: this.pos.y/moveScale}
    }

    IsInside(x, y) {
        return (Math.pow((x - this.p.x), 2) + Math.pow((y - this.p.y), 2)) <= Math.pow(particleRadius, 2)
    }
}
