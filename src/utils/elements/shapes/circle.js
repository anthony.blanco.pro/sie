import Shape from '@/utils/shape'

export default class Circle extends Shape {
    info = {
        center_x: 0,
        center_y: 0,
        radius: 0
    }

    constructor(circle) {
        super('CIRCLE')
        Object.assign(this, circle)
    }

    Draw(drawer, moveScale) {
        drawer.p.stroke(255)
        drawer.p.fill(0, 0, 0, 0)
        drawer.Circle(this.info.center_x, this.info.center_y, this.info.radius / moveScale)
    }

    SetPos(x, y) {
        this.info.center_x = x
        this.info.center_y = y
    }

    IsInside(x, y) {
        return (Math.pow((x - this.p.x), 2) + Math.pow((y - this.p.y), 2)) <= Math.pow(this.r, 2)
    }
}
