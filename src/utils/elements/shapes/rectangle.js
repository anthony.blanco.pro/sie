import Shape from '@/utils/shape'

export default class Rectangle extends Shape {
    constructor(info) {
        super('rectangle')
        Object.assign(this, info)
    }

    Draw(drawer, moveScale) {
        drawer.Rectangle(this.p.x / moveScale, this.p.y /moveScale, this.p0.x / moveScale, this.p0.y / moveScale)
    }

    SetPos(x, y) {
        super.SetPos(x, y)
    }

    IsInside(x, y) {
        return x >= this.p.x && x <= this.p0.x && y >= this.p.y && y <= this.p0.y
    }
}
