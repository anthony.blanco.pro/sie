export default {
    110: {
        title: 'Failed to open project',
        text: 'Unable to open wanted project, it seems to be corrupted',
        cancelable: true
    },
    105: {
        title: 'Failed to save project',
        text: 'Unable to save project, check file name and try again or restart application',
        cancelable: true
    }
}
