export default class Shape {
    /**
     * @param type {string}
     */
    constructor(type) {
        this.type = type
    }

    /**
     * virtual
     * Draw the elements on the grid
     * @param drawer {Drawer}
     * @param moveScale {Number}
     */
    // eslint-disable-next-line no-unused-vars
    Draw(drawer, moveScale) {
        // To implement in inherited class
    }

    /**
     * Set physical pos of the shape
     * @param x
     * @param y
     * @constructor
     */
    // eslint-disable-next-line no-unused-vars
    SetPos(x, y) {

    }

    /**
     * virtual
     * Return true if the given coordinates are on or in the Element
     * @param x {Number} x coordinate of point to check
     * @param y {Number} y coordinate of point to check
     * @return {boolean}
     */
    // eslint-disable-next-line no-unused-vars
    IsInside(x, y) {
        // To implement in inherited class
    }
}
