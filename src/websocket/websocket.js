const api_url = 'ws://127.0.0.1:8080/ws'

let socket = new WebSocket(api_url)
let socketReady = false;
let pending = new Map()
let sendPending = []

// List of all websocket requests
const API = {
    listAllProject: () => ({status: 'REQUEST', code: 100, route: 'list', data: ''}),
    loadProject: dir => ({status: 'REQUEST', code: 100, route: 'load', data: dir}),
    saveProject: jsonProj => ({status: 'REQUEST', code: 100, route: 'save', data: jsonProj}),
    renderProject: jsonProj => ({status: 'REQUEST', code: 100, route: 'render', data: jsonProj}),
}

/**
 * Send a request to the server
 * @param request {Object<WebSocket.API>}
 * @param autoClose {Boolean}
 */
function sendRequest(request) {
    return new Promise((resolve, reject) => {
        pending.set(request['route'], {resolve, reject})
        if (socketReady) {
            socket.send(JSON.stringify(request))
        } else {
            sendPending.push(JSON.stringify(request))
        }
    })
}

/**
 * Close a pending request
 * @param req
 */
function closeRequest(req) {
    pending.delete(req.route)
}

socket.onopen = () => {
    console.log('API Connection successful')
    socketReady = true
    if (sendPending.length !== 0) {
        console.log(`Sending ${sendPending.length} pending request..`)
        sendPending.forEach(req => socket.send(req))
    }
}

socket.onmessage = e => {
    let res = JSON.parse(e.data)

    if (res.status !== 'REQUEST') {
        if (pending.has(res.route)) {
            let pendingReq = pending.get(res.route)

            if (res.code === 100) pendingReq.resolve(res)
            else pendingReq.reject(res)

            if (res.status === 'CLOSE') closeRequest(res)
        }
    }
}

export default {
    API,
    sendRequest,
    closeRequest
}