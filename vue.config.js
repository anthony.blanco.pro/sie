const path = require('path')

module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  configureWebpack: {
    resolve: {
      extensions: [".js", ".vue", ".json", ".ts"],
      alias: {
        "@Util": path.resolve(__dirname, "src", "utils"),
        "@Compo": path.resolve(__dirname, "src", "components"),
        "@Views": path.resolve(__dirname, "src", "views"),
        "@Layout": path.resolve(__dirname, "src", "layouts"),
        "@Asset": path.resolve(__dirname, "src", "assets")
      }
    },
    devServer: {
      headers: { "Access-Control-Allow-Origin": "*" }
    }
  }
}